package main

import (
	"os"
	"os/signal"
	"syscall"
)

func main() {
	var quit = make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
}
