package domain

import (
	"errors"
	"fmt"
	"strconv"
)

const (
	AscendexHost       = "ascendex.com"
	AscendexStreamPath = "/1/api/pro/v1/stream"
)

var AscendexErrNoSymbol = errors.New("symbol is empty")
var AscendexErrNoCreds = errors.New("creds is empty")

type OrderBook struct {
	M      string        `json:"m"`
	Symbol string        `json:"symbol"`
	Data   OrderBookData `json:"data"`
}

type OrderBookData struct {
	Ts  int64    `json:"ts"`
	Bid []string `json:"bid"`
	Ask []string `json:"ask"`
}

func (o OrderBook) GetOrder() (askPrice float64, askAmount float64, bidPrice float64, bidAmount float64, err error) {
	if len(o.Data.Ask) != 2 {
		return 0, 0, 0, 0, errors.New(fmt.Sprintf("order.Data.Ask len less than 2 %v", o.Data.Ask))
	}

	if len(o.Data.Bid) != 2 {
		return 0, 0, 0, 0, errors.New(fmt.Sprintf("order.Data.Bid len less than 2 %v", o.Data.Bid))
	}

	askPrice, err = strconv.ParseFloat(o.Data.Ask[0], 64)
	if err != nil {
		return 0, 0, 0, 0, errors.New(fmt.Sprintf("cannot parse float askPrice in:%v, err:%v", o.Data.Ask[0], err))
	}

	askAmount, err = strconv.ParseFloat(o.Data.Ask[1], 64)
	if err != nil {
		return 0, 0, 0, 0, errors.New(fmt.Sprintf("cannot parse float askAmount in:%v, err:%v", o.Data.Ask[1], err))
	}

	bidPrice, err = strconv.ParseFloat(o.Data.Bid[0], 64)
	if err != nil {
		return 0, 0, 0, 0, errors.New(fmt.Sprintf("cannot parse float bidPrice in:%v, err:%v", o.Data.Bid[0], err))
	}

	bidAmount, err = strconv.ParseFloat(o.Data.Bid[1], 64)
	if err != nil {
		return 0, 0, 0, 0, errors.New(fmt.Sprintf("cannot parse float bidAmount in:%v, err:%v", o.Data.Bid[1], err))
	}

	return
}
