package tools

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"time"
)

func Encode(path, secret string) (string, string) {
	var timestamp = fmt.Sprintf("%d", time.Now().UnixNano()/int64(time.Millisecond))
	var message = timestamp + path
	var h = hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(message))

	return base64.StdEncoding.EncodeToString(h.Sum(nil)), timestamp
}
