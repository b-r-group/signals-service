package client

import (
	"brgroup/signals-service/internal/tools"
	"brgroup/signals-service/pkg/domain"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"net/url"
	"time"
)

type AscendexAPIClient struct {
	conn *websocket.Conn

	// symbol можно конечно убрать отсюда его, ибо он пока не используется нигде
	// но на будущее оставил
	symbol string

	// done канал для закрытия соединения по всем горутинам
	done chan struct{}

	apiKey string
	secret string
}

const (
	updateTime     = 2 * time.Second
	maxMessageSize = 512
)

func NewAscendexAPIClient(apiKey, secret string) (*AscendexAPIClient, error) {
	if secret == "" || apiKey == "" {
		return nil, domain.AscendexErrNoCreds
	}

	return &AscendexAPIClient{
		done: make(chan struct{}),
	}, nil
}

func (c *AscendexAPIClient) Connection() error {
	var u = url.URL{Scheme: "wss", Host: domain.AscendexHost, Path: domain.AscendexStreamPath}
	var signature, ts = tools.Encode("stream", c.secret)
	var ctx = context.Background()
	var err error

	// вроде не объязательно указывать заголовки, можно выпилить если что
	var header = http.Header{}
	header.Set("Accept", "application/json")
	header.Set("Content-Type", "application/json")
	header.Set("x-auth-key", c.apiKey)
	header.Set("x-auth-signature", signature)
	header.Set("x-auth-timestamp", ts)

	c.conn, _, err = websocket.DefaultDialer.DialContext(ctx, u.String(), header)
	if err != nil {
		return errors.New(fmt.Sprintf("Connection err:%v", err))
	}

	c.conn.SetReadLimit(maxMessageSize)

	return nil
}

func (c *AscendexAPIClient) Disconnect() {
	if err := c.conn.Close(); err != nil {
		log.Println("Disconnect error, cannot close connection err:", err)
		return
	}

	c.done <- struct{}{}
}

func (c *AscendexAPIClient) SubscribeToChannel(symbol string) error {
	if symbol == "" {
		return domain.AscendexErrNoSymbol
	}

	if err := c.conn.WriteMessage(websocket.TextMessage, []byte(c.getMessageForWS())); err != nil {
		return errors.New(fmt.Sprintf("SubscribeToChannel error, cannot write message, msg:%v, err:%v", c.getMessageForWS(), err))
	}

	return nil
}

func (c *AscendexAPIClient) getMessageForWS() string {
	return fmt.Sprintf(`{ "op": "sub", "ch":"bbo:%s" }`, c.symbol)
}

func (c *AscendexAPIClient) ReadMessagesFromChannel(ch chan<- BestOrderBook) {
	defer close(ch)
	for {
		select {
		case <-c.done:
			log.Println("ReadMessagesFromChannel disconnected")
			return
		case <-time.After(updateTime):
			var order domain.OrderBook
			_, message, err := c.conn.ReadMessage()
			if err != nil {
				log.Println("ReadMessagesFromChannel read error:", err)
				continue
			}

			if err := json.Unmarshal(message, &order); err != nil {
				log.Println("ReadMessagesFromChannel json error:", err)
				continue
			}

			askPrice, askAmount, bidPrice, bidAmount, err := order.GetOrder()
			if err != nil {
				log.Println("ReadMessagesFromChannel get order err:", err)
				continue
			}

			ch <- BestOrderBook{
				Ask: Order{
					Amount: askAmount,
					Price:  askPrice,
				},
				Bid: Order{
					Amount: bidAmount,
					Price:  bidPrice,
				},
			}
		}
	}
}

// WriteMessagesToChannel не совсем понял зачем нужен этот метод,
// как я понял он должен отправлять сообщения по сокету,
// если так, то это реализовано в SubscribeToChannel
func (c *AscendexAPIClient) WriteMessagesToChannel() {

}
