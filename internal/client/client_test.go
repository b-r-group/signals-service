package client

import (
	"brgroup/signals-service/pkg/domain"
	"log"
	"os"
	"testing"
	"time"
)

var apiKey, secret = os.Getenv("API_SECRET"), os.Getenv("PUBLIC_KEY")

func TestAscendexAPIClient_SubscribeToChannel_Failure(t *testing.T) {
	client, err := NewAscendexAPIClient(apiKey, secret)
	if err != nil {
		t.Error(err)
		return
	}
	err = client.Connection()
	if err != nil {
		t.Errorf("Failed to open WebSocket connection: %v", err)
		return
	}
	defer client.conn.Close()

	if err := client.SubscribeToChannel(""); err != domain.AscendexErrNoSymbol {
		t.Errorf("Got: %s, expected: %s", err, domain.AscendexErrNoSymbol)
	}
}

func TestAscendexAPIClient_Disconnect_Success(t *testing.T) {
	client, err := NewAscendexAPIClient(apiKey, secret)
	if err != nil {
		t.Error(err)
		return
	}
	err = client.Connection()
	if err != nil {
		t.Errorf("Failed to open WebSocket connection: %v", err)
		return
	}
	defer client.conn.Close()
	if client.conn == nil {
		t.Error("WebSocket connection is not initialized")
		return
	}
}

func TestAscendexAPIClient_Case(t *testing.T) {
	client, err := NewAscendexAPIClient(apiKey, secret)
	if err != nil {
		t.Error(err)
		return
	}

	if err := client.Connection(); err != nil {
		t.Error(err)
		return
	}

	err = client.SubscribeToChannel("BTC/USDT")
	if err != nil {
		t.Error(err)
		return
	}

	ch := make(chan BestOrderBook, 2)
	go client.ReadMessagesFromChannel(ch)

	go func() {
		time.Sleep(5 * time.Second)
		client.Disconnect()
	}()

	for bbo := range ch {
		log.Printf("%+v", bbo)
	}
}
